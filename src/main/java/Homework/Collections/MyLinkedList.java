package Homework.Collections;

import java.util.*;

public class MyLinkedList<T> implements List<T> {

    private static class Node<T> {
        T value;
        Node<T> next;
        Node<T> prev;

        public Node(T value) {
            this.value = value;
        }

        public Node(T value, Node<T> next) {
            this.value = value;
            this.next = next;
        }
    }

    private Node<T> head;
    private Node<T> tail;
    private int size;


    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return head == null;
    }

    @Override
    public boolean contains(Object o) {
        for (T t : this) {
            if (Objects.equals(t, o)) return true;
        }
        return false;
    }

    @Override
    public Iterator<T> iterator() {
        return new MyLinkedListIterator();
    }

    @Override
    public Object[] toArray() {
        return toArray(new Object[0]);
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        T1[] result = Arrays.copyOf(a, size);
        int i = 0;
        for (T elem : this) {
            result[i++] = (T1) elem;
        }
        return result;
    }

    @Override
    public boolean add(T elem) {
        Node<T> node = new Node<T>(elem);
        if (isEmpty()) {
            head = tail = node;
        } else {
            tail.next = node;
            node.prev = tail;
            tail = node;
        }
        size++;
        return true;
    }

    @Override
    public boolean remove(Object o) {
        int index = indexOf(o);
        if (index == -1) {
            return false;
        }
        remove(index);
        return true;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        for (Object o : c) {
            if (!contains(o)) return false;
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        boolean modified = false;
        for (Object o : c) {
            modified = add((T) o) || modified;
        }
        return modified;
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        boolean modified = false;
        for (Object o : c) {
            add(index, (T) o);
            modified = true;
        }
        return modified;
    }

    @Override
    public boolean removeAll(Collection<?> collection) {
        boolean flag = true;
        for (Object obj : collection) {
            flag &= remove(obj);
        }
        return flag;
    }

    @Override
    public boolean retainAll(Collection<?> collection) {
        if(collection == null)
        {
            throw new NullPointerException();
        }
        Iterator<T> iterator = iterator();

        boolean flag = false;
        while(iterator.hasNext())
        {
            if(!collection.contains(iterator.next()))
            {
                iterator.remove();
                flag = true;
            }
        }

        return flag;
    }


    @Override
    public void clear() {
        head = null;
        size = 0;
    }

    @Override
    public T get(int index) {
        Node<T> node = getNode(index);
        return node.value;
    }

    @Override
    public T set(int index, T element) {
        Node<T> node = getNode(index);
        node.value = element;
        return element;
    }

    @Override
    public void add(int index, T element) {
        if (index == 0) {
            head = new Node(element, head);
        } else {
            Node node = getNode(index - 1);
            node.next = new Node(element, node.next);
        }
        size++;
    }

    @Override
    public T remove(int index) {
        Node<T> node = getNode(index);
        removeNode(node);
        return node.value;
    }

    @Override
    public int indexOf(Object o) {
        Node node = head;
        for (int i = 0; i < size; i++) {
            if (equals(o, node.value)) {
                return i;
            }
            node = node.next;
        }
        return -1;
    }

    @Override
    public int lastIndexOf(Object o) {
        Node node = head;
        int index = -1;
        for (int i = 0; i < size; i++) {
            if (equals(o, node.value)) {
                index = i;
            }
            node = node.next;
        }
        return index;
    }

    @Override
    public ListIterator<T> listIterator() {
        return null; //do not do it
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        return null; //do not do it
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        if (fromIndex < 0 || toIndex >= size || fromIndex > toIndex) {
            throw new IndexOutOfBoundsException();
        }
        MyLinkedList<T> list = new MyLinkedList<>();

        Node<T> cur = getNode(fromIndex);
        for (int i = fromIndex; i < toIndex; i++) {
            list.add(cur.value);
            cur = cur.next;
        }

        return list;
    }

    private class MyLinkedListIterator implements Iterator<T> {
        private Node<T> cur;

        public MyLinkedListIterator() {
            cur = new Node<>(null);
            cur.next = head;
        }

        @Override
        public boolean hasNext() {
            return cur.next != null;
        }

        @Override
        public T next() {
            cur = cur.next;
            T value = cur.value;
            return value;
        }

        @Override
        public void remove() {
            removeNode(cur);
        }
    }

    private boolean equals(Object target, Object element) {
        if (target == null) {
            return element == null;
        } else {
            return target.equals(element);
        }
    }

    private void removeNode(Node<T> cur) {
        if (cur.prev == null) {
            head = cur.next;
        } else {
            cur.prev.next = cur.next;
        }
        if (cur.next == null) {
            tail = cur.prev;
        } else {
            cur.next.prev = cur.prev;
        }
        size--;
    }

    private Node<T> getNode(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException();
        }
        Node node = head;
        for (int i = 0; i < index; i++) {
            node = node.next;
        }
        return node;
    }
}

