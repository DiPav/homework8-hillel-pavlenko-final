package Homework.Collections;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;


class MyLinkedListTest {
    public static final String LIST_STRING_TEMPLATE = "[Apple, Blueberries, Carrot, Donut, Eggs, Fig]";
    List<String> list = new MyLinkedList<>();

    @BeforeEach
    void setup() {
        list.add("Apple");
        list.add("Blueberries");
        list.add("Carrot");
        list.add("Donut");
        list.add("Eggs");
        list.add("Fig");
    }

    @Test
    void shouldGiveSize() {
        assertEquals(list.size(), 6);
    }

    @Test
    void shouldRemoveByValue() {
        list.remove("Apple");
        assertEquals(list.size(), 5);
        assertEquals(list.get(0), "Blueberries");
    }

    @Test
    void shouldIterate() {
        List<String> temp = new ArrayList<>();
        temp.add("Apple");
        temp.add("Blueberries");
        temp.add("Carrot");
        temp.add("Donut");
        temp.add("Eggs");
        temp.add("Fig");

        for (int i = 0; i < list.size(); i++) {
            assertEquals(list.get(i), temp.get(i));
        }
    }

    @Test
    void shouldGiveACorrectString() {
        System.out.println(Arrays.toString(list.toArray()));
        assertEquals(Arrays.toString(list.stream().sorted().toArray()), LIST_STRING_TEMPLATE);
    }

    @Test
    void shouldBeCorrectRemoveAllMethod() {
        List<String> listTmp = new ArrayList<>();
        listTmp.add("Apple");
        listTmp.add("Blueberries");
        listTmp.add("Carrot");
        listTmp.add("Donut");

        assertTrue(list.removeAll(listTmp));

        assertEquals(Arrays.toString(list.toArray()), "[Eggs, Fig]");
    }

    @Test
    void shouldClear() {
        list.clear();

        assertEquals(list.size(), 0);
    }

    @Test
    void shouldVerifyTheEmptiness() {
        assertFalse(list.isEmpty());
    }

    @Test
    void shouldRemoveByIndex() {
        list.remove(2);
        assertEquals(list.size(), 5);
        assertEquals(list.get(2), "Donut");
    }

    @Test
    void shouldFindIndexByValue() {
        assertEquals(list.indexOf("Blueberries"), 1);
        assertEquals(list.indexOf("Camry"), -1);
    }

    @Test
    void shouldFindByValue() {
        assertTrue(list.contains("Blueberries"));
        assertFalse(list.contains("Camry"));
    }

    @Test
    void shouldFindAllElementsFromOtherList() {
        List<String> linkedList = new MyLinkedList<>();
        linkedList.add("Apple");
        linkedList.add("Blueberries");
        linkedList.add("Carrot");
        linkedList.add("Donut");
        linkedList.add("Eggs");
        linkedList.add("Fig");

        assertTrue(list.containsAll(linkedList));

        linkedList.add("Mustang");

        assertFalse(list.containsAll(linkedList));
    }

    @Test
    void shouldGiveTheElementByIndex() {
        assertEquals(list.get(3), "Donut");
    }

    @Test
    void shouldSetTheElementByIndex() {
        list.set(2, "E-Tron");
        assertEquals(list.get(2), "E-Tron");
    }

}
